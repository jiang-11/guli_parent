package com.guigu.ExceptionHandler;


import com.atguigu.commonutils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author jiang--
 * @date2020/7/2710:59
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {
    //指定返回什么异常
    @ExceptionHandler(Exception.class)
    //为了返回数据
    @ResponseBody
    public R error(Exception e){
        e.printStackTrace();
        return R.error().message("执行了统一的的异常处理..");
    }
}
